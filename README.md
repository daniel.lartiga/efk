# efk

Demo: Elasticsearch + Fluentd + Kibana, using docker compose

## Elasticsearch
 
Port `9200` is for API, port `9300` is for node communication inside elastic cluster

In this example, a single-node cluster was configured


## Kibana

http://localhost:5601/app/kibana